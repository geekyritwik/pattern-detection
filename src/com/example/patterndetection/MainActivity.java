package com.example.patterndetection;

import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.SurfaceView;

public class MainActivity extends Activity implements CvCameraViewListener2 {
    
	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
    
    @Override
    public void onManagerConnected(int status) {
    switch (status) {
case LoaderCallbackInterface.SUCCESS: {
    Log.i("TAG", "OpenCV loaded successfully");
    mOpenCvCameraView.enableView();
}
break;
default: {
super.onManagerConnected(status);
}
break;
}
}
};



private CameraBridgeViewBase mOpenCvCameraView;

private CascadeClassifier fistCascade;
private boolean flag = false;

private int fRows, fCols;

//private boolean gestureFlag = false;
//	private int resetCount = 0;
// String gest;

private static int fCount =0;
private static String handBlock,gestFinal;
private  static int checkDup[] = {0,0,0,0,0,0,0,0,0};
private static String Gesture;

private boolean firstFrame = true;

protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_main);

if (!OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_2, this,
mLoaderCallback))
return;
this.mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.HelloOpenCvView);
this.mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
this.mOpenCvCameraView.setCvCameraViewListener(this);

//	palmCascade = new CascadeClassifier();

fistCascade = new CascadeClassifier();
//		if (!palmCascade.load(Environment.getExternalStorageDirectory()
//				.getAbsolutePath() + "/palm.xml"))
//			Log.e("Palm Load", "Unsuccessful");

if (!fistCascade.load(Environment.getExternalStorageDirectory()
.getAbsolutePath() + "/fist.xml"))
Log.e("Fist Load", "Unsuccessful");



}

@Override
public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu; this adds items to the action bar if it is present.
getMenuInflater().inflate(R.menu.main, menu);
return true;
}

@Override
public void onCameraViewStarted(int width, int height) {

}

@Override
public void onCameraViewStopped() {
// TODO Auto-generated method stub

}

@Override
public void onPause() {
super.onPause();
if (mOpenCvCameraView != null)
mOpenCvCameraView.disableView();
}

public void onDestroy() {
super.onDestroy();
if (mOpenCvCameraView != null)
mOpenCvCameraView.disableView();
}

@Override
public void onResume() {
super.onResume();

OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, this,
mLoaderCallback);
}

@Override
public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
// TODO Auto-generated method stub
Mat frame = inputFrame.gray();

if(firstFrame == true)
{
fCols = frame.cols();
fRows = frame.rows();
firstFrame = false;
}

detectFist(frame);

//System.gc();

Core.rectangle (frame,new  Point(fCols*0.05,fRows*0.05), new Point(fCols*0.35,fRows*0.35),new Scalar(0,255,0), 3, 8, 0);
Core.rectangle (frame, new Point(fCols*0.41,fRows*0.05), new Point(fCols*0.58,fRows*0.35),new Scalar(0,255,0), 3, 8, 0);
Core. rectangle (frame,new Point(fCols*0.64,fRows*0.05), new Point(fCols*0.95,fRows*0.35),new Scalar(0,255,0), 3, 8, 0);
Core. rectangle (frame,new Point(fCols*0.05,fRows*0.41), new Point(fCols*0.35,fRows*0.58),new Scalar(0,255,0), 3, 8, 0);
Core.rectangle (frame,new Point(fCols*0.41,fRows*0.41), new Point(fCols*0.58,fRows*0.58),new Scalar(0,255,0), 3, 8, 0);
Core.rectangle (frame,new Point(fCols*0.64,fRows*0.41), new Point(fCols*0.95,fRows*0.58),new Scalar(0,255,0), 3, 8, 0);
Core.rectangle (frame,new Point(fCols*0.05,fRows*0.62), new Point(fCols*0.35,fRows*0.95),new Scalar(0,255,0), 3, 8, 0);
Core.rectangle (frame,new Point(fCols*0.41,fRows*0.62), new Point(fCols*0.58,fRows*0.95),new Scalar(0,255,0), 3, 8, 0);
Core.rectangle (frame,new Point(fCols*0.64,fRows*0.62), new Point(fCols*0.95,fRows*0.95),new Scalar(0,255,0), 3, 8, 0);

Mat mat = new Mat();

Core.flip(frame, mat, 1);

Core.putText(mat,gestFinal,new Point(30,30),0,1,new Scalar(255,0,255),1,8,false);

Core.putText(mat,Gesture,new Point(30,fRows-30),0,1,new Scalar(255,0,255),1,8,false);

//Core.putText(mat, gest, new Point(10,50), 0, 1, new Scalar(255),1, 8,false);


return mat;
}




//	Point detectPalm(Mat frame) {
//		MatOfRect palm = new MatOfRect();
//		Imgproc.equalizeHist(frame, frame);
//
//		palmCascade.detectMultiScale(frame, palm, 1.1, 2,
//				0 | Objdetect.CASCADE_SCALE_IMAGE, new Size(100, 100),
//				new Size(200, 200));
//
//		List<Rect> list = palm.toList();
//
//		Point center = null;
//
//		for (int i = 0; i < list.size(); i++) {
//			center = new Point(list.get(i).x + list.get(i).width * 0.5,
//					list.get(i).y + list.get(i).height * 0.5);
//
//			Core.ellipse(
//					frame,
//					center,
//					new Size(list.get(i).width * 0.5, list.get(i).height * 0.5),
//					0, 0, 360, new Scalar(255, 0, 255), 4, 8, 0);
//		}
//
//		return center;
//	}

Point detectFist(Mat frame) {
MatOfRect fist = new MatOfRect();
Imgproc.equalizeHist(frame, frame);

fistCascade.detectMultiScale(frame, fist, 1.1, 2,
0 | Objdetect.CASCADE_SCALE_IMAGE, new Size(100, 100),
new Size(200, 200));

List<Rect> list = fist.toList();

Point center = new Point(0, 0);

// if (list.size() != 0)
// Log.e("FIST COUNT", "" + list.size());

for (int i = 0; i < list.size(); i++)
{
center = new Point(list.get(i).x + list.get(i).width * 0.5,
list.get(i).y + list.get(i).height * 0.5);

Core.ellipse(
frame,
center,
new Size(list.get(i).width * 0.5, list.get(i).height * 0.5),
0, 0, 360, new Scalar(255, 0, 255), 4, 8, 0);
}

if(fCount > 60)
fCount = 0;
else if(fCount!=0)
{
if(center.x !=0 && center.y !=0)
{
handBlock = handBlock.concat(nineBlock( center, fRows, fCols));
}
else
fCount++;
}
else
{
//if(center!=Point(0,0))
{
gestFinal = handBlock;
Gesture = whatsmygest(gestFinal);
handBlock = "";
for(int i=0;i<9;i++)
checkDup[i]=0;
fCount++;
}
}
return center;
}

String whatsmygest(String gestinput)
{
int  value = 0;

try{
value = (int) Integer.valueOf(gestinput);
}
catch(Exception e)
{
value =0;
}
switch(value)
{
case 123:
case 456:
case 789: return "SWIPE RIGHT";
case 321:
case 654:
case 987: return "SWIPE LEFT";
case 147:
case 258:
case 369: return "SWIPE DOWN";
case 741:
case 852:
case 963: return "SWIPE UP";
case 1589:
case 1569:
case 1259:
case 1459:
case 159: return "TOP TO BOTTOM \\ ";

case 9851:
case 9651:
case 9521:
case 9541:

case 951: return "BOTTOM TO TOP \\ ";

case 3257:
case 3657:
case 3587:
case 3547:

case 357: return "TOP TO BOTTOM / ";

case 7523:
case 7563:
case 7853:
case 7453:
case 753: return "BOTTOM TO TOP / ";
}
return "";
}

//	String consize(String gestString)
//	 {
//		 String test = gestString;
//
//		 if(test == null) return "";
//
//		test.replaceAll("0", "");
//
//		Log.e("CONSIZE"," : "+   test);
//
//		 return test;
//	 }


String nineBlock( Point center, int FRows, int FCols )
{
if(center.x >= FCols*0.05 )
{
if(center.x < FCols*0.35)
{
if(center.y >= FRows*0.05)
{
if(center.y < FRows*0.35)
{

if(checkDup[2]!=0)
return "";
else
{
checkDup[2]++;
return "3";
}
}
else if(center.y >FRows*0.41 && center.y < FRows*0.58)
{

if(checkDup[5]!=0)
return "";
else
checkDup[5]++;
return "6";
}
else if(center.y > FRows*0.64 && center.y < FRows*0.95)
{

if(checkDup[8]!=0)
return "";
else
checkDup[8]++;
return "9";
}
}
}
else if(center.x > FCols*0.41 && center.x < FCols*0.58)
{
if(center.y >= FRows*0.05)
{
if(center.y < FRows*0.35)
{

if(checkDup[1]!=0)
return "";
else
checkDup[1]++;
return "2";
}
else if(center.y > FRows*0.41 && center.y < FRows*0.58)
{

if(checkDup[4]!=0)
return "";
else
checkDup[4]++;
return "5";
}
else if(center.y > FRows*0.64 && center.y < FRows*0.95)
{

if(checkDup[7]!=0)
return "";
else
checkDup[7]++;
return "8";
}
}
}
else if(center.x > FCols*0.64 && center.x < FCols*0.95)
{
if(center.y >= FRows*0.05)
{
if(center.y < FRows*0.35)
{

if(checkDup[0]!=0)
return "";
else
checkDup[0]++;
return "1";
}
else if(center.y > FRows*0.41 && center.y < FRows*0.58)
{

if(checkDup[3]!=0)
return "";
else
checkDup[3]++;
return "4";
}
else if(center.y > FRows*0.64 && center.y < FRows*0.95)
{

if(checkDup[6]!=0)
return "";
else
checkDup[6]++;
return "7";
}
}
}
}
return "";
}




static {
if (!OpenCVLoader.initDebug()) {
// Handle initialization error
}
}

}
